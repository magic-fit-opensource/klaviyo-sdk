import 'dart:io';

import 'package:dotenv/dotenv.dart';
import 'package:klaviyo_sdk/klaviyo_sdk.dart';
import 'package:test/test.dart';

void main() {
  late ProfilesApi profilesApi;

  setUp(() async {
    var env = DotEnv(includePlatformEnvironment: false)..load();

    final privateKey = env['PRIVATE_KEY']!;

    final klaviyoClient = KlaviyoHttpClient(privateKey, loggingEnabled: true);
    profilesApi = ProfilesApi(client: klaviyoClient);
  });

  test('Test get profiles', () async {
    var response =
        await profilesApi.getProfiles(filterEmail: 'engineering@magic.fit');
    expect(response.ok, true);
    expect(1, response.data?.length);
    expect('engineering@magic.fit', response.data?.first.attributes.email);
    expect(true, response.data?.first.subscribed);
  });

  test('Test get profile', () async {
    var response = await profilesApi.getProfile('01H39ZPEERM8K6B72GXV823QVT');
    expect(response.ok, true);
    expect(true, response.data != null);
    expect('chirag@magic.fit', response.data?.attributes.email);
  });

  test('Test create profile', () async {
    var response = await profilesApi.createProfile(
        externalId: 'externalId',
        email: 'engineering@magic.fit',
        firstName: 'FirstName',
        lastName: 'LastName');
    expect(response.ok, true);
    expect(true, response.data != null);
    expect('externalId', response.data?.attributes.externalId);
    expect('engineering@magic.fit', response.data?.attributes.email);
    expect('FirstName', response.data?.attributes.firstName);
    expect('LastName', response.data?.attributes.lastName);
  });

  test('Test unsubscribe profile', () async {
    var response =
        await profilesApi.unsubscribeProfiles(['engineering@magic.fit']);
    expect(response.statusCode, HttpStatus.accepted);
  });

  test('Test subscribe profile', () async {
    var response = await profilesApi
        .subscribeProfiles('RGHQQt', ['engineering@magic.fit']);
    expect(response.statusCode, HttpStatus.accepted);
  });

  test('Test update profile', () async {
    var getProfilesResponse =
        await profilesApi.getProfiles(filterEmail: 'engineering@magic.fit');
    expect(getProfilesResponse.ok, true);
    expect(1, getProfilesResponse.data?.length);

    final profile = getProfilesResponse.data!.first;

    var updateProfileResponse = await profilesApi.updateProfileProperties(
        profile.id, <String, String>{'test_prop_1': 'test_val_1'});

    expect(
      updateProfileResponse.data!.data.attributes.properties?['test_prop_1'],
      'test_val_1',
    );

    updateProfileResponse = await profilesApi.updateProfileProperties(
        profile.id, <String, String>{'test_prop_1': 'test_val_2'});

    expect(
      updateProfileResponse.data!.data.attributes.properties?['test_prop_1'],
      'test_val_2',
    );
  });
}
