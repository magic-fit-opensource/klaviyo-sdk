import 'package:json_annotation/json_annotation.dart';

part 'api_error.g.dart';

@JsonSerializable()
class ApiError {
  const ApiError({
    required this.id,
    required this.code,
    required this.title,
    required this.detail,
  });

  final String id;

  final String code;

  final String title;

  final String detail;

  Map<String, dynamic> toJson() => _$ApiErrorToJson(this);

  factory ApiError.fromJson(Map<String, dynamic> json) =>
      _$ApiErrorFromJson(json);
}

@JsonSerializable()
class ApiErrorSource {
  const ApiErrorSource({
    required this.pointer,
    required this.parameter,
  });

  final String pointer;

  final String parameter;

  Map<String, dynamic> toJson() => _$ApiErrorSourceToJson(this);

  factory ApiErrorSource.fromJson(Map<String, dynamic> json) =>
      _$ApiErrorSourceFromJson(json);
}

@JsonSerializable()
class ApiErrors {
  const ApiErrors({required this.errors});

  final List<ApiError> errors;

  Map<String, dynamic> toJson() => _$ApiErrorsToJson(this);

  factory ApiErrors.fromJson(Map<String, dynamic> json) =>
      _$ApiErrorsFromJson(json);
}
