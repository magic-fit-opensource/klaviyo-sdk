// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_error.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApiError _$ApiErrorFromJson(Map<String, dynamic> json) => ApiError(
      id: json['id'] as String,
      code: json['code'] as String,
      title: json['title'] as String,
      detail: json['detail'] as String,
    );

Map<String, dynamic> _$ApiErrorToJson(ApiError instance) => <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'title': instance.title,
      'detail': instance.detail,
    };

ApiErrorSource _$ApiErrorSourceFromJson(Map<String, dynamic> json) =>
    ApiErrorSource(
      pointer: json['pointer'] as String,
      parameter: json['parameter'] as String,
    );

Map<String, dynamic> _$ApiErrorSourceToJson(ApiErrorSource instance) =>
    <String, dynamic>{
      'pointer': instance.pointer,
      'parameter': instance.parameter,
    };

ApiErrors _$ApiErrorsFromJson(Map<String, dynamic> json) => ApiErrors(
      errors: (json['errors'] as List<dynamic>)
          .map((e) => ApiError.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ApiErrorsToJson(ApiErrors instance) => <String, dynamic>{
      'errors': instance.errors,
    };
