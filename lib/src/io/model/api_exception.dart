class ApiException implements Exception {
  final int statusCode;

  final String? body;


  ApiException(this.statusCode,
      {this.body});

  @override
  String toString() {
    return 'ApiException{statusCode: $statusCode, body: $body}';
  }
}
