import 'dart:convert';

import 'package:http/http.dart';

import 'api_error.dart';

class ApiResponse<T> {
  late final bool ok;

  int? statusCode;

  T? data;

  Map<String, String>? headers;

  ApiErrors? apiErrors;

  ApiResponse({this.statusCode, this.data, this.headers}) {
    ok = (statusCode ?? 500) < 400;
  }

  ApiResponse.fromResponse(Response response) {
    statusCode = response.statusCode;
    headers = response.headers;

    ok = statusCode! < 300;
  }

  ApiResponse.fromErrorResponse(Response response) {
    statusCode = response.statusCode;
    headers = response.headers;

    apiErrors = ApiErrors.fromJson(jsonDecode(response.body));
    ok = false;
  }

  @override
  String toString() {
    return 'ApiResponse{ok: $ok, statusCode: $statusCode, data: $data, '
        'headers: $headers}';
  }
}
