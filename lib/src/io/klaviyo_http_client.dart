import 'dart:async';
import 'dart:convert';
import 'dart:html' if (dart.library.io) 'dart:io';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

import 'model/api_exception.dart';

const _host = 'a.klaviyo.com';

/// Klaviyo http client. Takes care of authorization. Re-authorizes when token
/// expires and retries request
class KlaviyoHttpClient extends http.BaseClient {
  final log = Logger('KlaviyoSDK');

  late final http.Client _inner;

  final String userAgent = 'KlaviyoSDK/Dart-SDK';

  final String _privateKey;

  final bool _loggingEnabled;

  KlaviyoHttpClient(
    String privateKey, {
    http.Client? client,
    bool loggingEnabled = false,
  })  : _privateKey = privateKey,
        _loggingEnabled = loggingEnabled {
    _inner = client ?? http.Client();

    if (_loggingEnabled) {
      log.onRecord.listen((record) {
        print('${record.level.name}: ${record.time}: ${record.message}');
      });
    }
  }

  /// Creates URL with the given path and currently configured environment
  Uri getUrl(String path, {Map<String, dynamic>? queryParameters}) {
    if (queryParameters != null) {
      for (var key in queryParameters.keys.toList(growable: false)) {
        if (queryParameters[key] == null) {
          queryParameters.remove(key);
        }
      }
    }

    return Uri.https(_host, path, queryParameters);
  }

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    request.headers['sdk_name'] = 'Dart-SDK';

    return _inner.send(request);
  }

  @override
  Future<http.Response> head(
    Uri url, {
    Map<String, String>? headers,
  }) async {
    return _makeRequest('HEAD', url, headers: headers);
  }

  @override
  Future<http.Response> get(
    Uri url, {
    Map<String, String>? headers,
  }) async {
    return _makeRequest('GET', url, headers: headers);
  }

  @override
  Future<http.Response> post(
    Uri url, {
    Map<String, String>? headers,
    Object? body,
    Encoding? encoding,
  }) async {
    return _makeRequest('POST', url,
        headers: headers, body: body, encoding: encoding);
  }

  @override
  Future<http.Response> put(
    Uri url, {
    Map<String, String>? headers,
    Object? body,
    Encoding? encoding,
  }) async {
    return _makeRequest('PUT', url,
        headers: headers, body: body, encoding: encoding);
  }

  @override
  Future<http.Response> patch(
    Uri url, {
    Map<String, String>? headers,
    Object? body,
    Encoding? encoding,
  }) async {
    return _makeRequest('PATCH', url,
        headers: headers, body: body, encoding: encoding);
  }

  @override
  Future<http.Response> delete(
    Uri url, {
    Map<String, String>? headers,
    Object? body,
    Encoding? encoding,
  }) async {
    return _makeRequest('DELETE', url,
        headers: headers, body: body, encoding: encoding);
  }

  Future<http.Response> _makeRequest(
    String method,
    Uri url, {
    Map<String, String>? headers,
    Object? body,
    Encoding? encoding,
  }) async {
    headers ??= <String, String>{
      'Authorization': 'Klaviyo-API-Key $_privateKey',
      'Accept': 'application/json',
      'revision': '2023-06-15',
    };

    if (body != null) {
      headers['Content-Type'] ??= 'application/json';
    }

    if (_loggingEnabled) {
      log.info('Request: $method $url');
      log.info('Headers: $headers');
      log.info('Body: $body');
    }

    late http.Response response;

    switch (method) {
      case 'HEAD':
        response = await super.head(url, headers: headers);
        break;
      case 'GET':
        response = await super.get(url, headers: headers);
        break;
      case 'POST':
        response = await super
            .post(url, headers: headers, body: body, encoding: encoding);
        break;
      case 'PUT':
        response = await super
            .put(url, headers: headers, body: body, encoding: encoding);
        break;
      case 'PATCH':
        response = await super
            .patch(url, headers: headers, body: body, encoding: encoding);
        break;
      case 'DELETE':
        response = await super
            .delete(url, headers: headers, body: body, encoding: encoding);
        break;
    }

    if (_loggingEnabled) {
      log.info('Response: ${response.statusCode}');
      log.info('Headers: $headers');
      log.info('Body: ${response.body}');
    }

    if (response.statusCode >= HttpStatus.ok &&
        response.statusCode <= HttpStatus.noContent) {
      return response;
    }

    throw ApiException(response.statusCode, body: response.body);
  }
}
