import 'package:json_annotation/json_annotation.dart';
import 'package:klaviyo_sdk/src/profiles/model/profile.dart';

part 'get_profile_response.g.dart';

@JsonSerializable()
class GetProfileResponse {
  GetProfileResponse({required this.data});

  final Profile data;

  Map<String, dynamic> toJson() => _$GetProfileResponseToJson(this);

  factory GetProfileResponse.fromJson(Map<String, dynamic> json) =>
      _$GetProfileResponseFromJson(json);

  @override
  String toString() {
    return 'GetProfileResponse{data: $data}';
  }
}
