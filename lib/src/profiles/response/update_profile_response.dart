import 'package:json_annotation/json_annotation.dart';
import 'package:klaviyo_sdk/src/profiles/model/profile.dart';

part 'update_profile_response.g.dart';

@JsonSerializable()
class UpdateProfileResponse {
  UpdateProfileResponse({required this.data});

  final Profile data;

  Map<String, dynamic> toJson() => _$UpdateProfileResponseToJson(this);

  factory UpdateProfileResponse.fromJson(Map<String, dynamic> json) =>
      _$UpdateProfileResponseFromJson(json);

  @override
  String toString() {
    return 'UpdateProfileResponse{data: $data}';
  }
}
