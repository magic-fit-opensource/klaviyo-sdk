import 'package:json_annotation/json_annotation.dart';

import '../model/profile.dart';

part 'create_profile_response.g.dart';

@JsonSerializable()
class CreateProfileResponse {
  const CreateProfileResponse({required this.data});

  final Profile data;

  Map<String, dynamic> toJson() => _$CreateProfileResponseToJson(this);

  factory CreateProfileResponse.fromJson(Map<String, dynamic> json) =>
      _$CreateProfileResponseFromJson(json);

  @override
  String toString() {
    return 'CreateProfileResponse{data: $data}';
  }
}
