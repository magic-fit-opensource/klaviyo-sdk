import 'package:json_annotation/json_annotation.dart';
import 'package:klaviyo_sdk/src/profiles/model/profile.dart';

part 'get_profiles_response.g.dart';

@JsonSerializable()
class GetProfilesResponse {
  GetProfilesResponse({required this.data});

  final List<Profile> data;

  Map<String, dynamic> toJson() => _$GetProfilesResponseToJson(this);

  factory GetProfilesResponse.fromJson(Map<String, dynamic> json) =>
      _$GetProfilesResponseFromJson(json);

  @override
  String toString() {
    return 'GetProfilesResponse{data: $data}';
  }
}
