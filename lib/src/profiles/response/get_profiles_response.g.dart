// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_profiles_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetProfilesResponse _$GetProfilesResponseFromJson(Map<String, dynamic> json) =>
    GetProfilesResponse(
      data: (json['data'] as List<dynamic>)
          .map((e) => Profile.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetProfilesResponseToJson(
        GetProfilesResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
