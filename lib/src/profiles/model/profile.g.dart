// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Profiles _$ProfilesFromJson(Map<String, dynamic> json) => Profiles(
      data: (json['data'] as List<dynamic>)
          .map((e) => Profile.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ProfilesToJson(Profiles instance) => <String, dynamic>{
      'data': instance.data,
    };

Profile _$ProfileFromJson(Map<String, dynamic> json) => Profile(
      type: json['type'] as String,
      id: json['id'] as String,
      attributes: ProfileAttributes.fromJson(
          json['attributes'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProfileToJson(Profile instance) => <String, dynamic>{
      'type': instance.type,
      'id': instance.id,
      'attributes': instance.attributes,
    };

ProfileAttributes _$ProfileAttributesFromJson(Map<String, dynamic> json) =>
    ProfileAttributes(
      externalId: json['external_id'] as String?,
      email: json['email'] as String?,
      firstName: json['first_name'] as String?,
      lastName: json['last_name'] as String?,
      subscriptions: json['subscriptions'] == null
          ? null
          : Subscriptions.fromJson(
              json['subscriptions'] as Map<String, dynamic>),
      properties: json['properties'] as Map<String, dynamic>?,
    );

Map<String, dynamic> _$ProfileAttributesToJson(ProfileAttributes instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('email', instance.email);
  writeNotNull('external_id', instance.externalId);
  writeNotNull('first_name', instance.firstName);
  writeNotNull('last_name', instance.lastName);
  writeNotNull('subscriptions', instance.subscriptions);
  writeNotNull('properties', instance.properties);
  return val;
}

Subscriptions _$SubscriptionsFromJson(Map<String, dynamic> json) =>
    Subscriptions(
      email: json['email'] == null
          ? null
          : SubscriptionsEmail.fromJson(json['email'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SubscriptionsToJson(Subscriptions instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('email', instance.email);
  return val;
}

SubscriptionsEmail _$SubscriptionsEmailFromJson(Map<String, dynamic> json) =>
    SubscriptionsEmail(
      marketing: json['marketing'] == null
          ? null
          : SubscriptionsMarketing.fromJson(
              json['marketing'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SubscriptionsEmailToJson(SubscriptionsEmail instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('marketing', instance.marketing);
  return val;
}

SubscriptionsMarketing _$SubscriptionsMarketingFromJson(
        Map<String, dynamic> json) =>
    SubscriptionsMarketing(
      consent: json['consent'] as String,
    );

Map<String, dynamic> _$SubscriptionsMarketingToJson(
        SubscriptionsMarketing instance) =>
    <String, dynamic>{
      'consent': instance.consent,
    };
