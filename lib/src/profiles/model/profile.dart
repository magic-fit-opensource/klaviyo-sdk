import 'package:json_annotation/json_annotation.dart';

part 'profile.g.dart';

@JsonSerializable()
class Profiles {
  const Profiles({required this.data});

  final List<Profile> data;

  Map<String, dynamic> toJson() => _$ProfilesToJson(this);

  factory Profiles.fromJson(Map<String, dynamic> json) =>
      _$ProfilesFromJson(json);

  @override
  String toString() {
    return 'Profiles{data: $data}';
  }
}

@JsonSerializable()
class Profile {
  const Profile({
    required this.type,
    required this.id,
    required this.attributes,
  });

  final String type;

  final String id;

  final ProfileAttributes attributes;

  bool get subscribed =>
      attributes.subscriptions?.email?.marketing?.consent == 'SUBSCRIBED';

  Map<String, dynamic> toJson() => _$ProfileToJson(this);

  factory Profile.fromJson(Map<String, dynamic> json) =>
      _$ProfileFromJson(json);

  @override
  String toString() {
    return 'Profile{type: $type, id: $id, attributes: $attributes}';
  }
}

@JsonSerializable()
class ProfileAttributes {
  const ProfileAttributes({
    this.externalId,
    this.email,
    this.firstName,
    this.lastName,
    this.subscriptions,
    this.properties,
  });

  final String? email;

  @JsonKey(name: 'external_id')
  final String? externalId;

  @JsonKey(name: 'first_name')
  final String? firstName;

  @JsonKey(name: 'last_name')
  final String? lastName;

  final Subscriptions? subscriptions;

  final Map<String, dynamic>? properties;

  Map<String, dynamic> toJson() => _$ProfileAttributesToJson(this);

  factory ProfileAttributes.fromJson(Map<String, dynamic> json) =>
      _$ProfileAttributesFromJson(json);

  @override
  String toString() {
    return 'ProfileAttributes{email: $email, externalId: $externalId, '
        'firstName: $firstName, lastName: $lastName, subscriptions: '
        '$subscriptions, properties: $properties}';
  }
}

@JsonSerializable()
class Subscriptions {
  const Subscriptions({this.email});

  final SubscriptionsEmail? email;

  Map<String, dynamic> toJson() => _$SubscriptionsToJson(this);

  factory Subscriptions.fromJson(Map<String, dynamic> json) =>
      _$SubscriptionsFromJson(json);

  @override
  String toString() {
    return 'Subscriptions{email: $email}';
  }
}

@JsonSerializable()
class SubscriptionsEmail {
  const SubscriptionsEmail({this.marketing});

  final SubscriptionsMarketing? marketing;

  Map<String, dynamic> toJson() => _$SubscriptionsEmailToJson(this);

  factory SubscriptionsEmail.fromJson(Map<String, dynamic> json) =>
      _$SubscriptionsEmailFromJson(json);

  @override
  String toString() {
    return 'SubscriptionsEmail{marketing: $marketing}';
  }
}

@JsonSerializable()
class SubscriptionsMarketing {
  const SubscriptionsMarketing({required this.consent});

  final String consent;

  Map<String, dynamic> toJson() => _$SubscriptionsMarketingToJson(this);

  factory SubscriptionsMarketing.fromJson(Map<String, dynamic> json) =>
      _$SubscriptionsMarketingFromJson(json);

  @override
  String toString() {
    return 'SubscriptionsMarketing{consent: $consent}';
  }
}
