import 'dart:convert';
import 'dart:io';

import 'package:klaviyo_sdk/src/io/io.dart';
import 'package:klaviyo_sdk/src/io/model/api_response.dart';
import 'package:klaviyo_sdk/src/profiles/request/create_profile_request.dart';
import 'package:klaviyo_sdk/src/profiles/request/subscribe_profiles_request.dart';
import 'package:klaviyo_sdk/src/profiles/request/unsubscribe_profiles_request.dart';
import 'package:klaviyo_sdk/src/profiles/request/update_profile_request.dart';

import 'model/profile.dart';
import 'response/create_profile_response.dart';
import 'response/get_profile_response.dart';
import 'response/get_profiles_response.dart';

export 'model/profile.dart';

class ProfilesApi {
  const ProfilesApi({required KlaviyoHttpClient client}) : _client = client;

  final KlaviyoHttpClient _client;

  Future<ApiResponse<List<Profile>?>> getProfiles(
      {String? filterEmail, int pageSize = 20}) async {
    var uri = _client.getUrl('/api/profiles', queryParameters: {
      if (filterEmail != null) 'filter': 'equals(email,"$filterEmail")',
      'page[size]': pageSize.toString(),
    });

    var response = await _client.get(uri);

    if (response.statusCode == HttpStatus.ok) {
      final profile = GetProfilesResponse.fromJson(jsonDecode(response.body));
      return ApiResponse(
          statusCode: response.statusCode,
          data: profile.data,
          headers: response.headers);
    }

    return ApiResponse.fromErrorResponse(response);
  }

  Future<ApiResponse<Profile?>> getProfile(String id) async {
    var uri = _client.getUrl('/api/profiles/$id');

    var response = await _client.get(uri);

    if (response.statusCode == HttpStatus.ok) {
      final profile = GetProfileResponse.fromJson(jsonDecode(response.body));
      return ApiResponse(
          statusCode: response.statusCode,
          data: profile.data,
          headers: response.headers);
    }

    return ApiResponse.fromErrorResponse(response);
  }

  Future<ApiResponse<Profile?>> createProfile({
    required String externalId,
    required String email,
    required String firstName,
    required String lastName,
  }) async {
    var uri = _client.getUrl('/api/profiles');

    final request = CreateProfileRequest.from(
      externalId: externalId,
      email: email,
      firstName: firstName,
      lastName: lastName,
    );

    var body = jsonEncode(request.toJson());

    var response = await _client.post(uri, body: body);

    if (response.statusCode == HttpStatus.created) {
      final profile = CreateProfileResponse.fromJson(jsonDecode(response.body));
      return ApiResponse(
          statusCode: response.statusCode,
          data: profile.data,
          headers: response.headers);
    }

    return ApiResponse.fromErrorResponse(response);
  }

  Future<ApiResponse> subscribeProfiles(String listId, List<String> emails,
      {String? customSource}) async {
    var uri = _client.getUrl('/api/profile-subscription-bulk-create-jobs');

    final request = SubscribeProfilesRequest(
        data: SubscribeProfilesRequestData(
            attributes: SubscribeProfilesRequestAttributes(
      listId: listId,
      subscriptions: emails
          .map((e) => SubscribeProfilesRequestSubscription(email: e))
          .toList(),
      customSource: customSource,
    )));

    var body = jsonEncode(request.toJson());

    var response = await _client.post(uri, body: body);

    if (response.statusCode == HttpStatus.accepted) {
      return ApiResponse(
          statusCode: response.statusCode, headers: response.headers);
    }

    return ApiResponse.fromErrorResponse(response);
  }

  Future<ApiResponse> unsubscribeProfiles(List<String> emails) async {
    var uri = _client.getUrl('/api/profile-unsubscription-bulk-create-jobs');

    final request = UnsubscribeProfilesRequest(
        data: UnsubscribeProfilesRequestData(
            attributes: UnsubscribeProfilesRequestAttributes(emails: emails)));

    var body = jsonEncode(request.toJson());

    var response = await _client.post(uri, body: body);

    if (response.statusCode == HttpStatus.accepted) {
      return ApiResponse(
          statusCode: response.statusCode, headers: response.headers);
    }

    return ApiResponse.fromErrorResponse(response);
  }

  Future<ApiResponse<GetProfileResponse>> updateProfileProperties(
      String id, Map<String, String> properties) async {
    var uri = _client.getUrl('/api/profiles/$id');

    final request = UpdateProfileRequest.from(
      id: id,
      attributes: UpdateProfileRequestAttributes(
        properties: properties,
      ),
    );

    var body = jsonEncode(request.toJson());

    var response = await _client.patch(uri, body: body);

    if (response.statusCode == HttpStatus.ok) {
      final profile = GetProfileResponse.fromJson(jsonDecode(response.body));

      return ApiResponse<GetProfileResponse>(
        statusCode: response.statusCode,
        headers: response.headers,
        data: profile,
      );
    }

    return ApiResponse.fromErrorResponse(response);
  }
}
