// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subscribe_profiles_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubscribeProfilesRequest _$SubscribeProfilesRequestFromJson(
        Map<String, dynamic> json) =>
    SubscribeProfilesRequest(
      data: SubscribeProfilesRequestData.fromJson(
          json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SubscribeProfilesRequestToJson(
        SubscribeProfilesRequest instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

SubscribeProfilesRequestData _$SubscribeProfilesRequestDataFromJson(
        Map<String, dynamic> json) =>
    SubscribeProfilesRequestData(
      attributes: SubscribeProfilesRequestAttributes.fromJson(
          json['attributes'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SubscribeProfilesRequestDataToJson(
        SubscribeProfilesRequestData instance) =>
    <String, dynamic>{
      'type': instance.type,
      'attributes': instance.attributes,
    };

SubscribeProfilesRequestAttributes _$SubscribeProfilesRequestAttributesFromJson(
        Map<String, dynamic> json) =>
    SubscribeProfilesRequestAttributes(
      listId: json['list_id'] as String,
      subscriptions: (json['subscriptions'] as List<dynamic>)
          .map((e) => SubscribeProfilesRequestSubscription.fromJson(
              e as Map<String, dynamic>))
          .toList(),
      customSource: json['custom_source'] as String?,
    );

Map<String, dynamic> _$SubscribeProfilesRequestAttributesToJson(
    SubscribeProfilesRequestAttributes instance) {
  final val = <String, dynamic>{
    'list_id': instance.listId,
    'subscriptions': instance.subscriptions,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('custom_source', instance.customSource);
  return val;
}

SubscribeProfilesRequestSubscription
    _$SubscribeProfilesRequestSubscriptionFromJson(Map<String, dynamic> json) =>
        SubscribeProfilesRequestSubscription(
          email: json['email'] as String,
        );

Map<String, dynamic> _$SubscribeProfilesRequestSubscriptionToJson(
        SubscribeProfilesRequestSubscription instance) =>
    <String, dynamic>{
      'email': instance.email,
      'channels': instance.channels,
    };
