// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unsubscribe_profiles_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UnsubscribeProfilesRequest _$UnsubscribeProfilesRequestFromJson(
        Map<String, dynamic> json) =>
    UnsubscribeProfilesRequest(
      data: UnsubscribeProfilesRequestData.fromJson(
          json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UnsubscribeProfilesRequestToJson(
        UnsubscribeProfilesRequest instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

UnsubscribeProfilesRequestData _$UnsubscribeProfilesRequestDataFromJson(
        Map<String, dynamic> json) =>
    UnsubscribeProfilesRequestData(
      attributes: UnsubscribeProfilesRequestAttributes.fromJson(
          json['attributes'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UnsubscribeProfilesRequestDataToJson(
        UnsubscribeProfilesRequestData instance) =>
    <String, dynamic>{
      'type': instance.type,
      'attributes': instance.attributes,
    };

UnsubscribeProfilesRequestAttributes
    _$UnsubscribeProfilesRequestAttributesFromJson(Map<String, dynamic> json) =>
        UnsubscribeProfilesRequestAttributes(
          emails: (json['emails'] as List<dynamic>)
              .map((e) => e as String)
              .toList(),
        );

Map<String, dynamic> _$UnsubscribeProfilesRequestAttributesToJson(
        UnsubscribeProfilesRequestAttributes instance) =>
    <String, dynamic>{
      'emails': instance.emails,
    };
