import 'package:json_annotation/json_annotation.dart';

part 'update_profile_request.g.dart';

@JsonSerializable()
class UpdateProfileRequest {
  const UpdateProfileRequest(this.data);

  UpdateProfileRequest.from(
      {required String id, required UpdateProfileRequestAttributes attributes})
      : data = UpdateProfileRequestData(id: id, attributes: attributes);

  final UpdateProfileRequestData data;

  Map<String, dynamic> toJson() => _$UpdateProfileRequestToJson(this);

  factory UpdateProfileRequest.fromJson(Map<String, dynamic> json) =>
      _$UpdateProfileRequestFromJson(json);

  @override
  String toString() {
    return 'UpdateProfileRequest{data: $data}';
  }
}

@JsonSerializable()
class UpdateProfileRequestData {
  UpdateProfileRequestData({
    required this.id,
    this.type = 'profile',
    required this.attributes,
  });

  final String type;

  final String id;

  final UpdateProfileRequestAttributes attributes;

  Map<String, dynamic> toJson() => _$UpdateProfileRequestDataToJson(this);

  factory UpdateProfileRequestData.fromJson(Map<String, dynamic> json) =>
      _$UpdateProfileRequestDataFromJson(json);

  @override
  String toString() {
    return 'UpdateProfileRequestData{type: $type, id: $id, attributes: $attributes}';
  }
}

@JsonSerializable()
class UpdateProfileRequestAttributes {
  UpdateProfileRequestAttributes({this.properties});

  final Map<String, String>? properties;

  Map<String, dynamic> toJson() => _$UpdateProfileRequestAttributesToJson(this);

  factory UpdateProfileRequestAttributes.fromJson(Map<String, dynamic> json) =>
      _$UpdateProfileRequestAttributesFromJson(json);

  @override
  String toString() {
    return 'UpdateProfileRequestAttributes{properties: $properties}';
  }
}
