// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_profile_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateProfileRequest _$CreateProfileRequestFromJson(
        Map<String, dynamic> json) =>
    CreateProfileRequest(
      data: CreateProfileRequestData.fromJson(
          json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CreateProfileRequestToJson(
        CreateProfileRequest instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

CreateProfileRequestData _$CreateProfileRequestDataFromJson(
        Map<String, dynamic> json) =>
    CreateProfileRequestData(
      attributes: CreateProfileRequestDataAttributes.fromJson(
          json['attributes'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CreateProfileRequestDataToJson(
        CreateProfileRequestData instance) =>
    <String, dynamic>{
      'type': instance.type,
      'attributes': instance.attributes,
    };

CreateProfileRequestDataAttributes _$CreateProfileRequestDataAttributesFromJson(
        Map<String, dynamic> json) =>
    CreateProfileRequestDataAttributes(
      externalId: json['external_id'] as String,
      email: json['email'] as String,
      firstName: json['first_name'] as String,
      lastName: json['last_name'] as String,
    );

Map<String, dynamic> _$CreateProfileRequestDataAttributesToJson(
        CreateProfileRequestDataAttributes instance) =>
    <String, dynamic>{
      'external_id': instance.externalId,
      'email': instance.email,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
    };
