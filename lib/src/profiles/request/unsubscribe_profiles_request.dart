import 'package:json_annotation/json_annotation.dart';

part 'unsubscribe_profiles_request.g.dart';

@JsonSerializable()
class UnsubscribeProfilesRequest {
  UnsubscribeProfilesRequest({required this.data});

  UnsubscribeProfilesRequestData data;

  Map<String, dynamic> toJson() => _$UnsubscribeProfilesRequestToJson(this);

  factory UnsubscribeProfilesRequest.fromJson(Map<String, dynamic> json) =>
      _$UnsubscribeProfilesRequestFromJson(json);

  @override
  String toString() {
    return 'UnsubscribeProfilesRequest{data: $data}';
  }
}

@JsonSerializable()
class UnsubscribeProfilesRequestData {
  UnsubscribeProfilesRequestData({required this.attributes});

  @JsonKey(includeToJson: true)
  final String type = 'profile-unsubscription-bulk-create-job';

  final UnsubscribeProfilesRequestAttributes attributes;

  Map<String, dynamic> toJson() => _$UnsubscribeProfilesRequestDataToJson(this);

  factory UnsubscribeProfilesRequestData.fromJson(Map<String, dynamic> json) =>
      _$UnsubscribeProfilesRequestDataFromJson(json);

  @override
  String toString() {
    return 'UnsubscribeProfilesRequest{type: $type, attributes: $attributes}';
  }
}

@JsonSerializable()
class UnsubscribeProfilesRequestAttributes {
  UnsubscribeProfilesRequestAttributes({
    required this.emails,
  });

  final List<String> emails;

  Map<String, dynamic> toJson() =>
      _$UnsubscribeProfilesRequestAttributesToJson(this);

  factory UnsubscribeProfilesRequestAttributes.fromJson(
          Map<String, dynamic> json) =>
      _$UnsubscribeProfilesRequestAttributesFromJson(json);

  @override
  String toString() {
    return 'UnsubscribeProfilesRequestAttributes{emails: $emails}';
  }
}
