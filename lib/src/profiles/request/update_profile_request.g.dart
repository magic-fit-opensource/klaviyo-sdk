// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_profile_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateProfileRequest _$UpdateProfileRequestFromJson(
        Map<String, dynamic> json) =>
    UpdateProfileRequest(
      UpdateProfileRequestData.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UpdateProfileRequestToJson(
        UpdateProfileRequest instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

UpdateProfileRequestData _$UpdateProfileRequestDataFromJson(
        Map<String, dynamic> json) =>
    UpdateProfileRequestData(
      id: json['id'] as String,
      type: json['type'] as String? ?? 'profile',
      attributes: UpdateProfileRequestAttributes.fromJson(
          json['attributes'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UpdateProfileRequestDataToJson(
        UpdateProfileRequestData instance) =>
    <String, dynamic>{
      'type': instance.type,
      'id': instance.id,
      'attributes': instance.attributes,
    };

UpdateProfileRequestAttributes _$UpdateProfileRequestAttributesFromJson(
        Map<String, dynamic> json) =>
    UpdateProfileRequestAttributes(
      properties: (json['properties'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(k, e as String),
      ),
    );

Map<String, dynamic> _$UpdateProfileRequestAttributesToJson(
    UpdateProfileRequestAttributes instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('properties', instance.properties);
  return val;
}
