import 'package:json_annotation/json_annotation.dart';

part 'subscribe_profiles_request.g.dart';

@JsonSerializable()
class SubscribeProfilesRequest {
  SubscribeProfilesRequest({required this.data});

  final SubscribeProfilesRequestData data;

  Map<String, dynamic> toJson() => _$SubscribeProfilesRequestToJson(this);

  factory SubscribeProfilesRequest.fromJson(Map<String, dynamic> json) =>
      _$SubscribeProfilesRequestFromJson(json);

  @override
  String toString() {
    return 'SubscribeProfilesRequest{data: $data}';
  }
}

@JsonSerializable()
class SubscribeProfilesRequestData {
  SubscribeProfilesRequestData({required this.attributes});

  @JsonKey(includeToJson: true)
  final String type = 'profile-subscription-bulk-create-job';

  final SubscribeProfilesRequestAttributes attributes;

  Map<String, dynamic> toJson() => _$SubscribeProfilesRequestDataToJson(this);

  factory SubscribeProfilesRequestData.fromJson(Map<String, dynamic> json) =>
      _$SubscribeProfilesRequestDataFromJson(json);

  @override
  String toString() {
    return 'SubscribeProfilesRequestData{type: $type, attributes: $attributes}';
  }
}

@JsonSerializable()
class SubscribeProfilesRequestAttributes {
  SubscribeProfilesRequestAttributes({
    required this.listId,
    required this.subscriptions,
    this.customSource,
  });

  @JsonKey(name: 'list_id')
  final String listId;

  final List<SubscribeProfilesRequestSubscription> subscriptions;

  @JsonKey(name: 'custom_source')
  final String? customSource;

  Map<String, dynamic> toJson() =>
      _$SubscribeProfilesRequestAttributesToJson(this);

  factory SubscribeProfilesRequestAttributes.fromJson(
          Map<String, dynamic> json) =>
      _$SubscribeProfilesRequestAttributesFromJson(json);

  @override
  String toString() {
    return 'SubscribeProfilesRequestAttributes{listId: $listId, '
        'subscriptions: $subscriptions}';
  }
}

@JsonSerializable()
class SubscribeProfilesRequestSubscription {
  SubscribeProfilesRequestSubscription({required this.email});

  final String email;

  @JsonKey(includeToJson: true)
  final Map<String, List<String>> channels = {
    'email': ['MARKETING'],
  };

  Map<String, dynamic> toJson() =>
      _$SubscribeProfilesRequestSubscriptionToJson(this);

  factory SubscribeProfilesRequestSubscription.fromJson(
          Map<String, dynamic> json) =>
      _$SubscribeProfilesRequestSubscriptionFromJson(json);

  @override
  String toString() {
    return 'SubscribeProfilesRequestSubscription{profileId: $email, '
        'channels: $channels}';
  }
}
