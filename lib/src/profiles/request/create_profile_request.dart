import 'package:json_annotation/json_annotation.dart';

part 'create_profile_request.g.dart';

@JsonSerializable()
class CreateProfileRequest {
  CreateProfileRequest({
    required this.data,
  });

  CreateProfileRequest.from({
    required String externalId,
    required String email,
    required String firstName,
    required String lastName,
  }) {
    data = CreateProfileRequestData(
        attributes: CreateProfileRequestDataAttributes(
      externalId: externalId,
      email: email,
      firstName: firstName,
      lastName: lastName,
    ));
  }

  late final CreateProfileRequestData data;

  Map<String, dynamic> toJson() => _$CreateProfileRequestToJson(this);

  factory CreateProfileRequest.fromJson(Map<String, dynamic> json) =>
      _$CreateProfileRequestFromJson(json);

  @override
  String toString() {
    return 'CreateProfileRequest{data: $data}';
  }
}

@JsonSerializable()
class CreateProfileRequestData {
  const CreateProfileRequestData({required this.attributes});

  @JsonKey(includeToJson: true)
  final String type = 'profile';

  final CreateProfileRequestDataAttributes attributes;

  Map<String, dynamic> toJson() => _$CreateProfileRequestDataToJson(this);

  factory CreateProfileRequestData.fromJson(Map<String, dynamic> json) =>
      _$CreateProfileRequestDataFromJson(json);

  @override
  String toString() {
    return 'CreateProfileRequestData{type: $type, attributes: $attributes}';
  }
}

@JsonSerializable()
class CreateProfileRequestDataAttributes {
  const CreateProfileRequestDataAttributes({
    required this.externalId,
    required this.email,
    required this.firstName,
    required this.lastName,
  });

  @JsonKey(name: 'external_id')
  final String externalId;

  final String email;

  @JsonKey(name: 'first_name')
  final String firstName;

  @JsonKey(name: 'last_name')
  final String lastName;

  Map<String, dynamic> toJson() =>
      _$CreateProfileRequestDataAttributesToJson(this);

  factory CreateProfileRequestDataAttributes.fromJson(
          Map<String, dynamic> json) =>
      _$CreateProfileRequestDataAttributesFromJson(json);

  @override
  String toString() {
    return 'CreateProfileRequestDataAttributes{externalId: $externalId, '
        'email: $email, firstName: $firstName, lastName: $lastName}';
  }
}
